'''
Created on 2013-11-07

@author: Smitty
The snake module holds an "enum" for the direction, and the snake-relevant functions
'''

#directions the snake can get. Python doesn't seem to have enums so I made a class
class Direction:
    LEFT = 1
    RIGHT = 2
    UP = 3
    DOWN = 4

#just a basic class to hold the X and Y coord for a block of the snake
class SnakeBlock:
    
    def __init__(self, x, y):   
        self.x = x
        self.y = y
         

class Snake:
    
    def Move(self):
        #loop through each block backwards and set its x and y coordinate to the previous
        #block in the sequence
        for i in range(len(self.blocks), 1, -1):
            self.blocks[i-1].x = self.blocks[i-2].x
            self.blocks[i-1].y = self.blocks[i-2].y
        
        if(self.direction == Direction.LEFT):
            self.blocks[0].x -= 10
            if(self.blocks[0].x < 0):
                self.Kill()
                
        elif(self.direction == Direction.RIGHT):            
            self.blocks[0].x += 10
            
            if(self.blocks[0].x >= self.maxx):
                self.Kill()
        elif(self.direction == Direction.UP):               
            self.blocks[0].y -= 10
            if(self.blocks[0].y < 0):
                self.Kill()
        elif(self.direction == Direction.DOWN):             
            self.blocks[0].y += 10
            if(self.blocks[0].y >= self.maxy):
                self.Kill()
                
        for i in range(1, len(self.blocks)):
            if(self.blocks[0].x == self.blocks[i].x and self.blocks[0].y == self.blocks[i].y):
                self.Kill()
                
    #it's game over, man!
    def Kill(self):
        self.alive = False
    
    def AddBlock(self):
        self.blocks.append(SnakeBlock(self.blocks[len(self.blocks) - 1].x, self.blocks[len(self.blocks) - 1].y))
        
        #300 x 300 screen means 900 blocks. If the user gets to 899 blocks, they've won
        if(len(self.blocks) >= 899):
            self.winner = True
            
    def __init__(self, maxx, maxy):
        self.direction = Direction.RIGHT
        self.blocks = [SnakeBlock(50, 50)]
        self.alive = True
        self.winner = False
        self.maxx = maxx
        self.maxy = maxy