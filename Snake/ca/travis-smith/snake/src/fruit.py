'''
Created on 2013-11-08

@author: Smitty
A class to store the fruit information.
'''

class Fruit:

    def __init__(self):
        import random
        
        #assigns a random X and Y to the fruit that is divisible by 10 so it aligns with the snake
        self.x = random.randrange(0, 300, 10)
        self.y = random.randrange(0, 300, 10)
        self.colour = (255, 0, 0)